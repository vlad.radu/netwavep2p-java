package NetWave;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class MiddleMan {
    private volatile static ArrayList<Peer> PeerList = new ArrayList<Peer>();
    private volatile static ObservableList<Song> localSongs = FXCollections.observableArrayList();
    private volatile static ArrayList<Song> sharedSongs = new ArrayList<>();

    static ObservableList<Song> getLocalSongs()
    {
        return localSongs;
    }

    static void setLocalSongs(ObservableList<Song> ls) { localSongs = ls;}

    static ArrayList<Peer> getPeerList() {return  PeerList;}

    static ArrayList<Song> getSharedSongs() { return sharedSongs;}

}
