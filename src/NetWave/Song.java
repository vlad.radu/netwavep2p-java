package NetWave;

import javafx.application.Platform;
import javafx.collections.ObservableMap;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;

public class Song implements PeerTreeItem{
    private String songUUID;
    private ObservableMap<String, Object> metadata;
    private String title,artist,album;
    private String uri;
    private Media songMedia;

    private boolean isLocal, isShared;

    private Duration duration;
    private Image cover;

    Song(String uri) {
        this.uri = uri;
        isLocal = true;
        this.songMedia = new Media(new File(this.uri).toURI().toString());
        System.out.println("GUI: Song constructor");
        MediaPlayer mp = new MediaPlayer(songMedia);
        mp.setOnReady(new Runnable() {
            @Override
            public void run() {
                if (mp.getStatus() == MediaPlayer.Status.READY) {
                    gatherMetadata();
                    Gui.playList.refresh();
                    Gui.peerSongList.refresh();
                }
            }
        });
        mp.setVolume(0);
        mp.play();
        mp.stop();
        songUUID = UUID.randomUUID().toString();
    }

    Song(String uri, String songUUID)
    {
        this(uri);
        this.songUUID = songUUID;
    }

    Song(String uri, String songUUID, Peer p)
    {
        isLocal = false;
        this.uri = uri;
        this.songMedia = new Media(uri);
        System.out.println("SONG: url " + this.uri);
        this.songUUID = songUUID;

        new Thread(new Runnable() {
            @Override
            public void run() {
                    try{
                        URL url = new URL("http://" + p.IP + ":" + Server.PORT + "/getInfo/" + songUUID);
                        System.out.println("SONG: " + url);
                        URLConnection con = url.openConnection();
                        HttpURLConnection http = (HttpURLConnection)con;
                        http.setRequestMethod("HEAD");
                        http.disconnect();
                        int stat = http.getResponseCode();
                        System.out.println("INFO RESP CODE " + stat );
            if (stat != 206)
            {
                System.out.println("ERROR");
                return;
            }
                        System.out.println(http.getHeaderFields().keySet());
                    for ( String key : http.getHeaderFields().keySet()) {
                        if (key == null) { continue; } // ignore strange null value
                          switch (key) {
                            case "Title":
                                title = http.getHeaderField(key);
                                System.out.println("HEAD" + title);
                                break;
                            case "Album":
                                album = http.getHeaderField(key);
                                System.out.println(album);
                                break;
                            case "Artist":
                                artist = http.getHeaderField(key);
                                System.out.println("HEAD" + artist);
                                break;
                            default:
                                break;
                        }
                    }
                }catch (IOException e)
                {
                    e.printStackTrace();
                }
                Gui.peerSongList.refresh();
            }
            }).start();
    }


    public void gatherMetadata()
    {
        duration = songMedia.getDuration();
        metadata = songMedia.getMetadata();
        for(String key : metadata.keySet())
        {
            switch (key)
            {
                case "title":
                    this.title = (String) metadata.get(key);
//                    System.out.println(metadata.get(key));
                    break;

                case "artist":
                    this.artist = (String) metadata.get(key);
//                    System.out.println(metadata.get(key));
                    break;

                case "album":
                    this.album = (String) metadata.get(key);
//                    System.out.println(metadata.get(key));
                    break;

                case "image":
                    this.cover = (Image) metadata.get(key);

                default:
                    break;
            }
//            System.out.println(metadata.get("artist"));
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    Gui.peerSongList.refresh();
                    Gui.playList.refresh();
                }
            });
        }
    }


    public Media getMediaObj() { return songMedia; }

    public Image getCover() { return cover; }

    public Duration getDuration() { return duration; }

    String getUri() {return this.uri; }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbum() {
        return album;
    }

    public String getSongUUID() {
        return songUUID;
    }

    public boolean isLocal() { return isLocal; }

    boolean isShared() { return isShared;}

    void setShared(boolean shared) { this.isShared = shared; }

    @Override
    public String toString() {
        return (isShared() ? "[S] " : "") + artist + " | " + title;
    }

    @Override
    public String getName() {
        return toString();
    }
}
