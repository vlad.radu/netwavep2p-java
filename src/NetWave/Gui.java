package NetWave;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.AudioSpectrumListener;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class Gui extends Application {
    static ListView playList;
    private Button stopBut, playBut, addBut,streamBut;
    private AreaChart visualizer;
    private Text playingBanner, time, streamStatus;
    private ImageView cover;
    private Slider seekBar;

    //    private MediaPlayer mediaPlayer; // Garbage collector protection
    private XYChart.Series series1;
    private MediaPlayer crtMp;
    private ContextMenu menu;
    private MenuItem share;
    private Label label;
    private TreeItem<PeerTreeItem> dummyRoot;

    static TreeView<PeerTreeItem> peerSongList;

    private Scene scene;
    private Parent root;

    @Override
    public void start(Stage stage) {
        stage.setTitle("NetWaveP2P");

        initialize();
        peerSongList.setRoot(dummyRoot);
        peerSongList.setShowRoot(false);

        Server.addNewPeerListener(new NewPeerListener() {
            @Override
            public void handlePeer() {
                Platform.runLater(new Runnable() {
                    @Override
                    synchronized public void run() {
                        Peer p = MiddleMan.getPeerList().get(MiddleMan.getPeerList().size()-1);
                        p.addOnDeathListener(new PeerListener() {
                            @Override
                            public void handle(Peer p) {
                                Iterator<TreeItem<PeerTreeItem>> iter =
                                        peerSongList.getRoot().getChildren().iterator();
                                while (iter.hasNext()) {
                                    TreeItem<PeerTreeItem> pt = iter.next();
                                    if (pt.getValue().equals(p)) {
                                        System.out.println("2 be removed");
                                        iter.remove();
                                    }
                                }
                                peerSongList.refresh();
                            }
                        });
                        TreeItem<PeerTreeItem> root = new TreeItem<>(p);
                        dummyRoot.getChildren().add(root);
                        root.setExpanded(true);
                        for (Song s : p.getSharedSongs())
                        {
//                            s.gatherMetadata();
                            root.getChildren().add(new TreeItem<>(s));
                            System.out.println(s);
                            peerSongList.refresh();
                        }
                    }
                });
            }
        });

        peerSongList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2 && (peerSongList.getSelectionModel().getSelectedItem().getValue()) instanceof  Song)
                {
                    System.out.println("Here");
                    Song s = (Song)peerSongList.getSelectionModel().getSelectedItem().getValue();
                    System.out.println("GUI: Clicked song " + s.getSongUUID() + " link" + s.getUri());
                    play(s);
                }
            }
        });

        streamBut.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                P2PService.running = true;
                streamStatus.setText("Streaming!");
            }
        });

        addBut.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                fileChooser();
            }
        });

        playBut.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                try {
                    System.out.println("GUI: status " + crtMp.getStatus());
                    switch (crtMp.getStatus()) {
                        case READY:
                        case PAUSED:
                        case STOPPED:
                            crtMp.play();
                            break;
                        case PLAYING:
                            crtMp.pause();
                            break;
                    }
                } catch (RuntimeException e) {
                    System.out.println("GUI: Null Media Warning");
                }
            }
        });

        stopBut.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(crtMp.getStatus() == MediaPlayer.Status.PLAYING ||
                        crtMp.getStatus() == MediaPlayer.Status.PAUSED)
                    crtMp.stop();
            }
        });

        seekBar.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                seekBar.setValueChanging(false);
//                System.out.println("here");
//                System.out.println(seekBar.getValue());
                seekBar.adjustValue(seekBar.getValue());
                crtMp.seek(Duration.seconds(seekBar.getValue()));
                seekBar.setValueChanging(true);
            }
        });

        playList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if(event.getClickCount() == 2)
                {
                    play((Song) playList.getSelectionModel().getSelectedItem());
                }
            }
        });

        label.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            synchronized public void handle(MouseEvent event) {
                Song s = (Song) playList.getSelectionModel().getSelectedItem();
                if (!s.isShared())
                {
                    s.setShared(true);
                    MiddleMan.getSharedSongs().add(s);
                    System.out.println(s.getSongUUID());
                }
                else
                {
                    MiddleMan.getSharedSongs().remove(s);
                    s.setShared(false);
                }
                playList.refresh();
                System.out.println(s);
            }
        });

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("save.dat")));
                    ArrayList<String[]> ss = new ArrayList<String[]>();
                    for (Song s : MiddleMan.getLocalSongs())
                    {
                        ss.add(new String[]{s.getUri(), s.getSongUUID()});
                    }
                    oos.writeObject(ss);
                }catch (IOException e)
                {
                    System.out.println("Cannot save config");
                    e.printStackTrace();
                }
            }
        });

        stage.setScene(scene);
        stage.show();
    }

    private void initialize() {
        try {
            this.root = FXMLLoader.load(getClass().getClassLoader().getResource("Interface.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        this.scene = new Scene(root);
        this.playBut = (Button) scene.lookup("#playBut");

        this.visualizer = (AreaChart) scene.lookup("#visualizer");
        visualizer.setAnimated(false);
        visualizer.getYAxis().setAutoRanging(false);
        NumberAxis num = (NumberAxis) visualizer.getYAxis();
        num.setLowerBound(0);
        num.setUpperBound(40);
        num.setTickUnit(10);

        this.stopBut = (Button) scene.lookup("#stopBut");
        this.playList = (ListView<Song>) scene.lookup("#playList");
        this.playingBanner = (Text) scene.lookup("#playingBanner");
        this.cover = (ImageView) scene.lookup("#cover");
        this.time = (Text) scene.lookup("#time");
        this.seekBar = (Slider) scene.lookup("#seekBar");
        this.addBut = (Button) scene.lookup("#add");
        this.menu = new ContextMenu();
        this.label = new Label("Share");
        this.peerSongList = (TreeView<PeerTreeItem>) scene.lookup("#peerSongList");
        this.dummyRoot = new TreeItem<PeerTreeItem>();
        this.share = new CustomMenuItem(label);
        this.streamBut = (Button) scene.lookup("#streamBut");
        this.streamStatus = (Text) scene.lookup("#streamStatus");
        menu.getItems().add(share);

//        menu.getItems().add(share)
        playList.setContextMenu(menu);
        load();

    }

    private void play(Song song) {
        if (!(crtMp == null)) crtMp.stop();
        crtMp = new MediaPlayer(song.getMediaObj());
        System.out.println("In play source: " + song.getMediaObj().getSource());

        crtMp.setAudioSpectrumNumBands(120);
        crtMp.setAudioSpectrumInterval(0.1);

        series1 = new XYChart.Series();

        crtMp.setAudioSpectrumListener(new AudioSpectrumListener() {
            public void spectrumDataUpdate(double timestamp, double duration, float[] magnitudes, float[] phases) {
                visualizer.getData().clear();
                series1.getData().clear();
                int crtTime = (int) crtMp.getCurrentTime().toSeconds();
                if (!seekBar.isPressed())
                    seekBar.adjustValue(crtMp.getCurrentTime().toSeconds());
                time.setText(
                        String.format("%d:%02d/%d:%02d",
                                crtTime / 60,
                                crtTime % 60,
                                (int) song.getDuration().toSeconds() / 60,
                                (int) song.getDuration().toSeconds() % 60)
                );
                for (int i = 0; i < crtMp.getAudioSpectrumNumBands() - 80; i++) {
                    series1.getData().add(new XYChart.Data(Integer.toString(i), (60 + magnitudes[i])));
                }
                visualizer.getData().addAll(series1);
            }
        });
        crtMp.setOnReady(new Runnable() {
            @Override
            public void run() {
                if (crtMp.getStatus() == MediaPlayer.Status.READY) {
                    if (!song.isLocal()) song.gatherMetadata();
                    System.out.println("GUI: Duration " + song.getDuration().toMinutes());
                    seekBar.setMax(song.getDuration().toSeconds());
                    seekBar.setMin(0);
                    cover.setImage(song.getCover());
                    playingBanner.setText("Now Playing: " + song.getArtist() + " - " + song.getTitle());
                    System.out.println("GUI: Tostring" + song.toString());
                }
                playList.refresh();
                peerSongList.refresh();
            }
        });
        crtMp.play();
        crtMp.setVolume(30);
    }

    private void load()
    {
        File sav = new File("save.dat");
        if (sav.exists())
        {
            try
            {
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(sav));
                ObservableList<Song> mm = MiddleMan.getLocalSongs();
                ArrayList<String[]> as = (ArrayList<String[]>) ois.readObject();
                for (String[] s : as)
                {
                    mm.add(new Song(s[0],s[1]));
                }
                playList.setItems(FXCollections.observableArrayList(MiddleMan.getLocalSongs()));
            }catch (IOException e)
            {
                System.out.println("Cannot open save.dat");
                e.printStackTrace();
            }catch (ClassNotFoundException ex)
            {
                System.out.println("Unknown class");
                ex.printStackTrace();
            }
        }


    }

    private void fileChooser() {
        ObservableList<Song> names = FXCollections.observableArrayList();
        FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter extentionFilter = new FileChooser.ExtensionFilter("MP3 Files (*.mp3)", "*.mp3");
        fileChooser.getExtensionFilters().add(extentionFilter);


        String userDirectoryString = System.getProperty("user.home");
        File userDirectory = new File(userDirectoryString);
        fileChooser.setInitialDirectory(userDirectory);


        List<File> chosenFile = fileChooser.showOpenMultipleDialog(null);

        for (File f : chosenFile) {
            MiddleMan.getLocalSongs().add(new Song(f.toString()));
        }
        names.addAll(MiddleMan.getLocalSongs());
        playList.setItems(names);
    }
}
