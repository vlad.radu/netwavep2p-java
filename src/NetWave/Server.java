package NetWave;

import com.sun.media.sound.ModelIdentifier;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import javafx.application.Platform;

import java.io.*;
import java.net.*;
import java.util.*;

public class Server extends Thread {

    Set<String> ipList;
    final public static int PORT = 50080;
    private static ArrayList<NewPeerListener> listeners = new ArrayList<NewPeerListener>();

    public Server()
    {
        ipList = new HashSet<String>();
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress("0.0.0.0",PORT), 0);
            server.createContext("/getSong", new songStreamHandler());
            server.createContext("/getInfo", new songInfoHandler());
            server.createContext("/songList", new songListHandler());
            server.start();
        }catch (Exception e)
        {
            System.out.println("Exception:");
            e.printStackTrace();
        }
        this.start();
    }

    public static void addNewPeerListener(NewPeerListener listener)
    {
        listeners.add(listener);
    }

    private void requestSongs(Peer p)
    {
        try {
            URL url = new URL("http://"+ p.IP + ":" + PORT +"/songList");
            URLConnection con = url.openConnection();
            HttpURLConnection http = (HttpURLConnection)con;
            http.setRequestMethod("GET");
            InputStream is = con.getInputStream();
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                    String songUUID = rd.readLine();
                    while (songUUID != null)
                    {
                        String songURL = "http://"+ p.getIP()+":" + PORT +"/getSong/"+songUUID;
                        System.out.println(songURL);
                        p.getSharedSongs().add(new Song(songURL,songUUID,p));
                        System.out.println("SERVER: Got " + songUUID + " link: " + songURL);
                        songUUID = rd.readLine();
                    }
                    }catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                    System.out.println("DONE WITH SONGS");

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            Gui.peerSongList.refresh();
                        }
                    });
                    listeners.forEach((el) -> el.handlePeer());
                }
            });
            t.start();
        }catch (MalformedURLException e)
        {
            System.out.println("Invalid url");
            e.printStackTrace();
        }catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    synchronized public void run(){
        // Multicast listener
        try  {
            MulticastSocket clientSocket = new MulticastSocket(Heartbeat.PORT);
            clientSocket.joinGroup(InetAddress.getByName(Heartbeat.INET_ADDR));

            while (true) {
                byte[] buf = new byte[256];
                DatagramPacket msgPacket = new DatagramPacket(buf, buf.length);
                clientSocket.receive(msgPacket);
                String ip = msgPacket.getAddress().getHostAddress();
                System.out.println("SERVER: recvd from " + ip);
//              && !ip.equals(InetAddress.getLocalHost().getHostAddress())
                if (ipList.add(ip) )
                {
                    Peer p = new Peer(ip, msgPacket.getAddress().getCanonicalHostName());
                    MiddleMan.getPeerList().add(p);
                    requestSongs(p);
                }

                Iterator<Peer> iter = MiddleMan.getPeerList().iterator();

                while (iter.hasNext())
                {
                    Peer p = iter.next();
                    if(!p.getIP().equals(ip))
                    {
                        if (!p.checkAlive(System.currentTimeMillis()))
                        {
                            iter.remove();
                        }
                    }
                    else p.updateTimect(System.currentTimeMillis());
                }

//                System.out.println("Received heartbeat from " + ip);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private class songInfoHandler implements HttpHandler
    {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String uriString = t.getRequestURI().toString();
            String reqCode = uriString.substring(uriString.indexOf("o/")+2);
            Headers h = t.getResponseHeaders();
            h.add("Server", "NetWave-u la Limbric");
            h.add("Accept-Ranges", "bytes");
            h.add("Connection", "Keep-Alive");
            for (Song s : MiddleMan.getSharedSongs()) {
                if (s.isShared() && (s.getSongUUID().equals(reqCode))) {
//                    System.out.println("SERVER: HEAD found " + s.getUri());
                    h.add("Title", s.getTitle());
                    h.add("Artist", s.getArtist());
                    h.add("Album", s.getAlbum());
                    break;
                }
            }
            t.sendResponseHeaders(206, -1);
//            t.close();
            System.out.println("SERVER: Served info");
        }
    }

    private class songListHandler implements HttpHandler
    {

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            Headers h = httpExchange.getResponseHeaders();
            h.add("Accept-Ranges", "bytes");
            h.add("Server", "NetWave-u la Limbric");
            h.add("Connection", "Keep-Alive");
            OutputStream reply = httpExchange.getResponseBody();
            StringBuilder songList = new StringBuilder();
            for(Song s : MiddleMan.getLocalSongs())
            {
                if(s.isShared())
                {
                    System.out.println("SERVER: sent " + s.getUri() + " UUID  " + s.getSongUUID());
                    String songData = s.getSongUUID() + "\n";
                    songList.append(songData);
                }
            }
            httpExchange.sendResponseHeaders(200,songList.length());
            reply.write(songList.toString().getBytes());
            reply.close();

            System.out.println("Server: Sent songList to" + httpExchange.getRemoteAddress());
        }
    }

    private class songStreamHandler implements HttpHandler {

        private void serveSong(HttpExchange t, Song s) throws IOException {
            Headers h = t.getResponseHeaders();
            h.add("Accept-Ranges", "bytes");
            h.add("Server", "NetWave-u la Limbric");
            h.add("Connection", "Keep-Alive");
            h.add("Content-Type", "audio/mpeg");
            h.add("Keep-Alive", "timeout=5, max=100");
            File file = new File(s.getUri());
            t.sendResponseHeaders(200, file.length());
            OutputStream os = t.getResponseBody();
            FileInputStream fs = new FileInputStream(file);
            final byte[] buffer = new byte[0x10000];
            int count = 0;
            while ((count = fs.read(buffer)) >= 0) {
                os.write(buffer, 0, count);
            }
            fs.close();
            os.close();
        }

        private void serveInfo(HttpExchange t, Song s) throws IOException
        {
            Headers h = t.getResponseHeaders();
            h.add("Server", "NetWave-u la Limbric");
                    System.out.println("SERVER: HEAD found " + s.getUri());
                    h.add("Title", s.getTitle());
                    h.add("Artist", s.getArtist());
            t.sendResponseHeaders(200,-1);
//            t.sendResponseHeaders(200,0);
            System.out.println("SERVER: Served info");

        }

        synchronized public void handle(HttpExchange t) throws IOException
        {
            String uriString = t.getRequestURI().toString();
            System.out.println("Server: URI STRING " + uriString);
            String reqCode = uriString.substring(uriString.indexOf("g/")+2);
            System.out.println("Server: Request code " + reqCode);
            Song reqSong = null;
            for ( Song s : MiddleMan.getSharedSongs())
            {
                if (s.isShared() && (s.getSongUUID().equals(reqCode)))
                {
                    System.out.println("SERVER: found " + s.getUri());
                    reqSong = s;
                    break;
                }
            }
            if (reqSong == null)
            {
                System.out.println("Server: Received invalid UUID");
                return;
            }
            if ("GET".equals(t.getRequestMethod()))
            {
                if (t.getRequestHeaders().containsKey("Metadata")) serveInfo(t, reqSong);
                else  serveSong(t, reqSong);
            }
            else if (t.getRequestMethod().equals("HEAD"))
            {
                Headers h =  t.getResponseHeaders();
                h.add("Accept-Ranges", "bytes");
                h.add("Server", "NetWave-u la Limbric");
                h.add("Connection", "Keep-Alive");
                h.add("Content-Type", "audio/mpeg");
                t.sendResponseHeaders(206,  -1); // 206 Partial Content
                t.close();
                }
            }
        }
    }
