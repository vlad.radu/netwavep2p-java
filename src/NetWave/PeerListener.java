package NetWave;

interface PeerListener {
    void handle(Peer p);
}
