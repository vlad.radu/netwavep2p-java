package NetWave;

import java.util.ArrayList;

public class Peer implements PeerTreeItem{
    final String IP;
    private int TTL = 3;
    private boolean isAlive = true;

    private static ArrayList<PeerListener> listeners = new ArrayList<PeerListener>();

    private long timeCt;

    private String netName;

    private ArrayList<Song> sharedSongs = new ArrayList<>();

    public Peer(String ip, String netName)
    {
        IP = ip;
        timeCt = System.currentTimeMillis();
        System.out.println("Peer: " + netName);
        this.netName = netName;
    }

    public Peer(String ip, String netName, PeerListener p)
    {
        this(ip,netName);
        this.addOnDeathListener(p);
    }

    ArrayList<Song> getSharedSongs()
    {
        return  sharedSongs;
    }

    public String getIP() {
        return IP;
    }

    public void updateTimect(long time) {timeCt = time;}

    public boolean checkAlive(long time)
    {
        long delta = (time - timeCt);
        if ((time - timeCt) > 6000) {
            TTL--;
            if (TTL <= 0)
            {
                isAlive = false;
                listeners.forEach((el) -> el.handle(this));
            }
        }
        else TTL=3;

        System.out.println("SERVER | " + this.getName() +" : Delta: " + delta +"  TTL " + TTL + " time  " + time + "timeCt " + timeCt);
        return isAlive;
    }

    public void addOnDeathListener(PeerListener p)
    {
        listeners.add(p);
    }

    @Override
    public String toString() {
        return netName;
    }

    @Override
    public String getName() {
        return toString();
    }
}
