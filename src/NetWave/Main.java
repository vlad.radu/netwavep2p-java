
package NetWave;


import javafx.application.Application;

import javax.sound.midi.MidiDevice;

public class Main {

    public static void main(String[] Args)
    {
        Thread thread = new Thread(new P2PService());
        thread.start();
        Gui gui = new Gui();
        Application.launch(gui.getClass(),Args);
    }

}