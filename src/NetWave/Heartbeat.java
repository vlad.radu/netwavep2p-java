package NetWave;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Heartbeat implements Runnable{

    static final String INET_ADDR = "224.0.0.4";
    static final int PORT = 9090;
    private String msg = "";

    @Override
    public void run() {
        InetAddress addr = null;
        try {
            addr = InetAddress.getByName(INET_ADDR);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try (DatagramSocket serverSocket = new DatagramSocket()) {
            while (true) {

                DatagramPacket msgPacket = new DatagramPacket(msg.getBytes(),
                        msg.getBytes().length, addr, PORT);
                serverSocket.send(msgPacket);
//                System.out.println("beat");
                Thread.sleep(5000);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void setMsq(String msg) {
        this.msg = msg;
    }
}
