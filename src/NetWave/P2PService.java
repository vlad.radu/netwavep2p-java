package NetWave;

public class P2PService extends Thread{
    private static Server httpServer;
    private static Heartbeat udpHeartbeat;
    public static volatile boolean running = false;
    @Override
    public void run() {
        try {
            while(!running) {
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        httpServer = new Server();
        udpHeartbeat = new Heartbeat();
        new Thread(udpHeartbeat).start();
    }

}
